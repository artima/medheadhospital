package medheadhospital.controller;

import medheadhospital.service.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = GetNearestHospital.class)
class GetNearestHospitalTest {

    @MockBean
    private MedicalStaffService medicalStaffService;

    @MockBean
    private SpecialismsService specialismsService;

    @MockBean
    private HospitalService hospitalService;

    @MockBean
    private PatientsService patientsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetNearestHospital() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/nearesthospital/{name}/{speciality}", "Claire Marie", "Endodontie"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
