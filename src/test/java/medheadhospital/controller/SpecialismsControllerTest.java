package medheadhospital.controller;


import medheadhospital.service.MedicalStaffService;
import medheadhospital.service.SpecialismsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@WebMvcTest(controllers = SpecialismsController.class)
class SpecialismsControllerTest {

    @MockBean
    private MedicalStaffService medicalStaffService;

    @MockBean
    private SpecialismsService specialismsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetSpecialisms() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/specialisms")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}