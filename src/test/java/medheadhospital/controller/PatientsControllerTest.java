package medheadhospital.controller;

import medheadhospital.service.MedicalStaffService;
import medheadhospital.service.PatientsService;
import medheadhospital.service.SpecialismsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = PatientsController.class)
public class PatientsControllerTest {

    @MockBean
    private MedicalStaffService medicalStaffService;

    @MockBean
    private SpecialismsService specialismsService;

    @MockBean
    private PatientsService patientsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetSpecialities() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/patients")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
