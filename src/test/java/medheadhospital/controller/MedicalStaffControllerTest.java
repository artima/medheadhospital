package medheadhospital.controller;


import medheadhospital.service.MedicalStaffService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = MedicalStaffController.class)
public class MedicalStaffControllerTest {

    @MockBean
    private MedicalStaffService medicalStaffService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetGroups() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/medical_staff")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
