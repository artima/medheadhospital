package medheadhospital.repository;


import medheadhospital.model.MedicalStaff;
import medheadhospital.model.Hospital;
import medheadhospital.model.Specialisms;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class HospitalRepositoryTest {

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private SpecialismsRepository specialismsRepository;

    @Autowired
    private MedicalStaffRepository medicalStaffRepository;

    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)

    public void saveGroupSpecTest(){

        MedicalStaff medicalStaff = MedicalStaff.builder().specialtyGroup("Beverly Bashir").build();

        medicalStaffRepository.save(medicalStaff);

        boolean expected = medicalStaffRepository.selectExistsName("Beverly Bashir");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Speciality
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialityTest(){

        long id = 1;
        MedicalStaff medicalStaff = medicalStaffRepository.getById(id);

        Specialisms specialisms = Specialisms.builder()
                .specialismName("Soins intensifs")
                .medicalStaff(medicalStaff)
                .build();

        specialismsRepository.save(specialisms);

        boolean expected = specialismsRepository.selectExistsName("Soins intensifs");

        Assertions.assertThat(expected).isTrue();
    }
/*
    // JUnit test for savePatient
    @Order(3)
    @Test
    @Rollback(value = false)
    public void saveHospitalTest(){

        List<Specialisms> spec = specialismsRepository.findAll();

        Hospital hospital= Hospital.builder()
                .hospitalName("Saint Martin")
                .latitude(43.853458960795)
                .longitude(3.420394857384)
                .specialisms(spec)
                .build();

        hospitalRepository.save(hospital);

        boolean expected = hospitalRepository.selectExistsName("Saint Martin");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(4)
    public void getHospitalTest(){

        long id = 1;
        Hospital hospital = hospitalRepository.findById(id).get();

        Assertions.assertThat(hospital.getId()).isEqualTo(1);
    }


    @Test
    @Order(5)
    public void getListOfHospitalTest(){

        List<Hospital> hospital = hospitalRepository.findAll();

        Assertions.assertThat(hospital.size()).isGreaterThan(0);
    }

    @Test
    @Order(6)
    @Rollback(value = false)
    public void updateHospitalTest(){

        long id = 1;
        Hospital hospital = hospitalRepository.findById(id).get();

        hospital.setHospitalName("Saint Sebastien");

        Hospital hospitalUpdated = hospitalRepository.save(hospital);

        Assertions.assertThat(hospitalUpdated.getHospitalName()).isEqualTo("Saint Sebastien");
    }



    @Test
    @Order(7)
    @Rollback(value = false)
    public void deleteHospitalTest(){

        long id = 1;
        Hospital hospital = hospitalRepository.findById(id).get();

        hospitalRepository.delete(hospital);

        Hospital hospital1 = null;

        Optional<Hospital> optionalHospital = hospitalRepository.findByNewName("Saint Sebastien");

        if (optionalHospital.isPresent()){
            hospital1 = optionalHospital.get();
        }

        Assertions.assertThat(hospital1).isNull();
    }*/

}