package medheadhospital.repository;


import medheadhospital.model.MedicalStaff;
import medheadhospital.model.Specialisms;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SpecialismsRepositoryTest {

    @Autowired
    private MedicalStaffRepository medicalStaffRepository;

    @Autowired
    private SpecialismsRepository specialismsRepository;

    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        MedicalStaff medicalStaff = MedicalStaff.builder().specialtyGroup("Groupe dentaire").build();

        medicalStaffRepository.save(medicalStaff);

        boolean expected = medicalStaffRepository.selectExistsName("Groupe dentaire");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Specialisms
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialismsTest(){

        long id = 1;
        MedicalStaff medicalStaff = medicalStaffRepository.getById(id);

        Specialisms specialisms = Specialisms.builder()
                .specialismName("Soins intensifs")
                .medicalStaff(medicalStaff)
        .build();

        specialismsRepository.save(specialisms);

        boolean expected = specialismsRepository.selectExistsName("Soins intensifs");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(3)
    public void getSpecialismsTest(){

        long id = 1;
        Specialisms specialisms = specialismsRepository.findById(id).get();

        Assertions.assertThat(specialisms.getId()).isEqualTo(1);
    }


    @Test
    @Order(4)
    public void getListOfSpecialismsTest(){

        List<Specialisms> specialisms = specialismsRepository.findAll();

        Assertions.assertThat(specialisms.size()).isGreaterThan(0);
    }

    @Test
    @Order(5)
    @Rollback(value = false)
    public void updateSpecialismsTest(){

        long id = 1;
        Specialisms specialisms = specialismsRepository.findById(id).get();

        specialisms.setSpecialismName("Soins");

        Specialisms SpecialismsUpdated = specialismsRepository.save(specialisms);

        Assertions.assertThat(SpecialismsUpdated.getSpecialismName()).isEqualTo("Soins");
    }


    @Test
    @Order(6)
    @Rollback(value = false)
    public void deleteSpecialismsTest(){

        long id = 1;
        Specialisms specialisms = specialismsRepository.findById(id).get();

        specialismsRepository.delete(specialisms);

        Specialisms specialisms1 = null;

        Optional<Specialisms> optionalSpecialisms = specialismsRepository.findByName("Soins");

        if (optionalSpecialisms.isPresent()){
            specialisms1 = optionalSpecialisms.get();
        }

        Assertions.assertThat(specialisms1).isNull();
    }

}