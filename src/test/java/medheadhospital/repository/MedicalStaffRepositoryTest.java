package medheadhospital.repository;

import medheadhospital.model.MedicalStaff;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MedicalStaffRepositoryTest {

    @Autowired
    private MedicalStaffRepository medicalStaffRepository;

    // JUnit test for saveGroup

    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveMedicalStaffTest(){

        MedicalStaff medicalStaff = MedicalStaff.builder().specialtyGroup("Anesthésie").build();

        medicalStaffRepository.save(medicalStaff);

        boolean expected = medicalStaffRepository.selectExistsName("Anesthésie");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(2)
    public void getMedicalStaffsByIdTest(){

        long id = 1;
        MedicalStaff medicalStaff = medicalStaffRepository.findById(id).get();

        Assertions.assertThat(medicalStaff.getId()).isEqualTo(1);
    }

    @Test
    @Order(3)
    public void getAllMedicalStaffsTest(){

        List<MedicalStaff> MedicalStaff = medicalStaffRepository.findAll();

        Assertions.assertThat(MedicalStaff.size()).isGreaterThan(0);

    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateMedicalStaffTest(){

        long id = 1;
        MedicalStaff MedicalStaff = medicalStaffRepository.findById(id).get();

        MedicalStaff.setSpecialtyGroup("Second Anesthésie");

        MedicalStaff MedicalStaffUpdated = medicalStaffRepository.save(MedicalStaff);

        Assertions.assertThat(MedicalStaffUpdated.getSpecialtyGroup()).isEqualTo("Second Anesthésie");
    }

}