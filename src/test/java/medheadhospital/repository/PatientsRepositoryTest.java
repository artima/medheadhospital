package medheadhospital.repository;



import medheadhospital.model.MedicalStaff;
import medheadhospital.model.Patients;
import medheadhospital.model.Specialisms;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PatientsRepositoryTest {

    @Autowired
    private PatientsRepository patientsRepository;

    @Autowired
    private SpecialismsRepository specialismsRepository;

    @Autowired
    private MedicalStaffRepository medicalStaffRepository;


    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        MedicalStaff medicalStaff = MedicalStaff.builder().specialtyGroup("Groupe dentaire").build();

        medicalStaffRepository.save(medicalStaff);

        boolean expected = medicalStaffRepository.selectExistsName("Groupe dentaire");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Speciality
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialityTest(){

        long id = 1;
        MedicalStaff medicalStaff = medicalStaffRepository.getById(id);

        Specialisms specialisms = Specialisms.builder()
                .specialismName("Soins intensifs")
                .medicalStaff(medicalStaff)
                .build();

        specialismsRepository.save(specialisms);

        boolean expected = specialismsRepository.selectExistsName("Soins intensifs");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for savePatient
    @Order(3)
    @Test
    @Rollback(value = false)
    public void savePatientTest(){

        List<Specialisms> spec = specialismsRepository.findAll();

        Patients patients = Patients.builder()
                .firstName("Frida")
                .lastName("Kahlo")
                .latitude(42.853458960795)
                .longitude(3.120394857384)
                .specialisms(spec)
                .build();

        patientsRepository.save(patients);

        boolean expected = patientsRepository.selectExistsName("Kahlo");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(4)
    public void getPatientTest(){

        long id = 1;
        Patients patients = patientsRepository.findById(id).get();

        Assertions.assertThat(patients.getId()).isEqualTo(1);
    }


    @Test
    @Order(5)
    public void getListOfPatientTest(){

        List<Patients> patient = patientsRepository.findAll();

        Assertions.assertThat(patient.size()).isGreaterThan(0);
    }


    @Test
    @Order(6)
    @Rollback(value = false)
    public void updatePatientTest(){

        long id = 1;
        Patients patients = patientsRepository.findById(id).get();

        patients.setLastName("Kahlo");

        Patients patientUpdated = patientsRepository.save(patients);

        Assertions.assertThat(patientUpdated.getLastName()).isEqualTo("Kahlo");
    }



    @Test
    @Order(7)
    @Rollback(value = false)
    public void deletePatientTest(){

        long id = 1;
        Patients patient = patientsRepository.findById(id).get();

        patientsRepository.delete(patient);

        Patients patient1 = null;

        Optional<Patients> optionalPatient = patientsRepository.findByNewName("Kahlo");

        if (optionalPatient.isPresent()){
            patient1 = optionalPatient.get();
        }

        Assertions.assertThat(patient1).isNull();
    }

}