package medheadhospital.service;

import medheadhospital.model.MedicalStaff;

import java.util.List;

public interface MedicalStaffService {
    MedicalStaff saveMedicalStaff(MedicalStaff medicalStaff);
    List<MedicalStaff> getAllMedicalStaffs();
    MedicalStaff getMedicalStaffById(long id);
    MedicalStaff updateMedicalStaff(MedicalStaff medicalStaff, long id);
    void deleteMedicalStaff(long id);
}