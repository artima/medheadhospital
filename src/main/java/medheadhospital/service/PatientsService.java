package medheadhospital.service;

import medheadhospital.model.Patients;

import java.util.List;

public interface PatientsService {
    Patients savePatients(Patients Patients);
    List<Patients> getAllPatients();
    Patients getPatientsById(long id);
    Patients updatePatients(Patients Patients, long id);
    void deletePatients(long id);
    Patients findPatientByName(String name);
    Double getPatientLatitudeByName(String name);
    Double getPatientLongitudeName(String name);
}
