package medheadhospital.service.impl;

import medheadhospital.exception.ResourceNotFoundException;
import medheadhospital.model.MedicalStaff;
import medheadhospital.repository.MedicalStaffRepository;
import medheadhospital.service.MedicalStaffService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class MedicalStaffServiceImpl implements MedicalStaffService {

    @Autowired
    private MedicalStaffRepository medicalStaffRepository;

    public MedicalStaffServiceImpl(MedicalStaffRepository medicalStaffRepository) {

        this.medicalStaffRepository = medicalStaffRepository;
    }

    @Override
    public MedicalStaff saveMedicalStaff(MedicalStaff medicalStaff) {
        return medicalStaffRepository.save(medicalStaff);
    }

    @Override
    public List<MedicalStaff> getAllMedicalStaffs() {
        return medicalStaffRepository.findAll();
    }

    @Override
    public MedicalStaff getMedicalStaffById(long id) {
        return medicalStaffRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("MedicalStaff", "id", "id"));
    }

    @Override
    public MedicalStaff updateMedicalStaff(MedicalStaff medicalStaff, long id) {
        MedicalStaff existingMedicalStaff = medicalStaffRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("MedicalStaff", "id", "id"));
        medicalStaffRepository.save(existingMedicalStaff);
        return existingMedicalStaff;
    }

    @Override
    public void deleteMedicalStaff(long id) {
        medicalStaffRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("MedicalStaff", "id", "id"));
        medicalStaffRepository.deleteById(id);
    }
}
