package medheadhospital.service.impl;

import medheadhospital.exception.ResourceNotFoundException;
import medheadhospital.model.Specialisms;
import medheadhospital.repository.SpecialismsRepository;
import medheadhospital.service.SpecialismsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecialismsServiceImpl implements SpecialismsService {

    @Autowired
    private SpecialismsRepository specialismsRepository;

    public SpecialismsServiceImpl(SpecialismsRepository specialismsRepository) {

        this.specialismsRepository = specialismsRepository;
    }

    @Override
    public Specialisms saveSpecialisms(Specialisms Specialisms) {
        return specialismsRepository.save(Specialisms);
    }

    @Override
    public List<Specialisms> getAllSpecialisms() {
        return specialismsRepository.findAll();
    }

    @Override
    public Specialisms getSpecialismsById(long id) {
        return specialismsRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Specialisms", "id", "id"));
    }

    @Override
    public Specialisms updateSpecialisms(Specialisms Specialisms, long id) {
        Specialisms existingSpecialisms = specialismsRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Specialisms", "id", "id"));
        existingSpecialisms.setSpecialismName(Specialisms.getSpecialismName());
        specialismsRepository.save(existingSpecialisms);
        return existingSpecialisms;
    }

    @Override
    public void deleteSpecialisms(long id) {
        specialismsRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Specialisms", "id", "id"));
        specialismsRepository.deleteById(id);
    }

}
