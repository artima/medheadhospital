package medheadhospital.service.impl;

import medheadhospital.exception.ResourceNotFoundException;
import medheadhospital.model.Hospital;
import medheadhospital.repository.HospitalRepository;
import medheadhospital.service.HospitalService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService {

    private HospitalRepository hospitalRepository;

    public HospitalServiceImpl(HospitalRepository hospitalRepository) {

        this.hospitalRepository = hospitalRepository;
    }

    @Override
    public Hospital saveHospital(Hospital hospital) {
        return hospitalRepository.save(hospital);
    }

    @Override
    public List<Hospital> getAllHospitals() {
        return hospitalRepository.findAll();
    }

    @Override
    public Hospital getHospitalById(long id) {
        return hospitalRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Hospital", "id", "id"));
    }

    @Override
    public Hospital updateHospital(Hospital Hospital, long id) {
        Hospital existingHospital = hospitalRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Hospital", "id", "id"));
        hospitalRepository.save(existingHospital);
        return existingHospital;
    }

    @Override
    public void deleteHospital(long id) {
        hospitalRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Hospital", "id", "id"));
        hospitalRepository.deleteById(id);
    }

    @Override
    public Hospital findHospitalByName(String name) {
        return hospitalRepository.findByName(name);
    }

    @Override
    public Double getHospitalLatitudeByName(String name) {
        Hospital hospital = new Hospital();
        try{
            hospital = hospitalRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return hospital.getLatitude();
    }

    @Override
    public Double getHospitalLongitudeName(String name) {
        Hospital hospital = new Hospital();
        try{
            hospital = hospitalRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return hospital.getLongitude();
    }

    @Override
    public List<String> findHospitalBySpeciality(String speciality) {
        List<String> hospital = hospitalRepository.findBySpeciality(speciality);
        return hospital;
    }

}


