package medheadhospital.service.impl;

import medheadhospital.exception.ResourceNotFoundException;
import medheadhospital.model.Patients;
import medheadhospital.repository.PatientsRepository;
import medheadhospital.service.PatientsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientsServiceImpl implements PatientsService {

    private PatientsRepository patientsRepository;

    public PatientsServiceImpl(PatientsRepository patientsRepository) {

        this.patientsRepository = patientsRepository;
    }

    @Override
    public Patients savePatients(Patients Patients) {
        return patientsRepository.save(Patients);
    }

    @Override
    public List<Patients> getAllPatients() {
        return patientsRepository.findAll();
    }

    @Override
    public Patients getPatientsById(long id) {
        return patientsRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Patients", "id", "id"));
    }

    @Override
    public Patients updatePatients(Patients Patients, long id) {
        Patients existingPatients = patientsRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Patients", "id", "id"));
        existingPatients.setFirstName(Patients.getFirstName());
        existingPatients.setLastName(Patients.getLastName());
        existingPatients.setEmail(Patients.getEmail());
        patientsRepository.save(existingPatients);
        return existingPatients;
    }

    @Override
    public void deletePatients(long id) {
        patientsRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Patients", "id", "id"));
        patientsRepository.deleteById(id);
    }

    @Override
    public Patients findPatientByName(String name) {
        return patientsRepository.findByName(name);
    }

    @Override
    public Double getPatientLatitudeByName(String name) {

        Patients patient = new Patients();
        try{
            patient = patientsRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return patient.getLatitude();
    }

    @Override
    public Double getPatientLongitudeName(String name) {

        Patients patient = new Patients();
        try{
            patient = patientsRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return patient.getLongitude();
    }
}
