package medheadhospital.service;

import medheadhospital.model.Hospital;

import java.util.List;

public interface HospitalService {
    Hospital saveHospital(Hospital hospital);
    List<Hospital> getAllHospitals();
    Hospital getHospitalById(long id);
    Hospital updateHospital(Hospital hospital, long id);
    void deleteHospital(long id);
    Hospital findHospitalByName(String name);
    Double getHospitalLatitudeByName(String name);
    Double getHospitalLongitudeName(String name);
    List<String> findHospitalBySpeciality(String speciality);
}
