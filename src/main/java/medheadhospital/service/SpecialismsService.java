package medheadhospital.service;

import medheadhospital.model.Specialisms;

import java.util.List;

public interface SpecialismsService {
    Specialisms saveSpecialisms(Specialisms specialisms);
    List<Specialisms> getAllSpecialisms();
    Specialisms getSpecialismsById(long id);
    Specialisms updateSpecialisms(Specialisms specialisms, long id);
    void deleteSpecialisms(long id);
}
