package medheadhospital.controller;

import medheadhospital.model.Hospital;
import medheadhospital.service.HospitalService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/hospital")
public class HospitalController {

    private HospitalService hospitalService;//test

    public HospitalController(HospitalService hospitalService) {
        super();
        this.hospitalService = hospitalService;
    }

    //build create Hospital REST API
    @PostMapping
    public ResponseEntity<Hospital> saveHospital(@RequestBody Hospital hospital){
        return new ResponseEntity<Hospital>(hospitalService.saveHospital(hospital), HttpStatus.CREATED);
    }

    //build gey all Hospital REST API
    @GetMapping
    public List<Hospital> getAllHospitals() {
        return hospitalService.getAllHospitals();
    }

    //build get by ID Hospital REST API
    @GetMapping("{id}")
    public ResponseEntity<Hospital> getHospitalsById(@PathVariable("id") long HospitalId) {
        return new ResponseEntity<Hospital>(hospitalService.getHospitalById(HospitalId), HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public Hospital getHospitalByName(@PathVariable String name){
        return hospitalService.findHospitalByName(name);
    }

    //build update Hospital REST API
    @PutMapping("{id}")
    public ResponseEntity<Hospital> updateHospital(@PathVariable("id") long HospitalID,
                                                  @RequestBody Hospital hospital) {
        return new ResponseEntity<Hospital>(hospitalService.updateHospital(hospital, HospitalID), HttpStatus.OK);
    }

    //build delete Hospital REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteHospital(@PathVariable("id") long id) {
        hospitalService.deleteHospital(id);
        return new ResponseEntity<String>("Hospital deleted successfully!.", HttpStatus.OK);
    }

    @GetMapping("/v/{speciality}")
    public List<String> getHospitalBySpeciality(@PathVariable String speciality){
        return hospitalService.findHospitalBySpeciality(speciality);
    }
}
