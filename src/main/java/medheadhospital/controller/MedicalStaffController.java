package medheadhospital.controller;

import medheadhospital.model.MedicalStaff;
import medheadhospital.service.MedicalStaffService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("/api/medical_staff")
public class MedicalStaffController {

    @Autowired
    private MedicalStaffService medicalStaffService;

    public MedicalStaffController(MedicalStaffService medicalStaffService) {
        super();
        this.medicalStaffService = medicalStaffService;
    }

    //build create medicalStaff REST API
    @PostMapping
    public ResponseEntity<MedicalStaff> saveMedicalStaff(@RequestBody MedicalStaff medicalStaff){
        return new ResponseEntity<MedicalStaff>(medicalStaffService.saveMedicalStaff(medicalStaff), HttpStatus.CREATED);
    }

    //build gey all medicalStaff REST API
    @GetMapping
    public List<MedicalStaff> getAllMedicalStaffs() {
        return medicalStaffService.getAllMedicalStaffs();
    }

    //build get by ID medicalStaff REST API
    @GetMapping("{id}")
    public ResponseEntity<MedicalStaff> getMedicalStaffsById(@PathVariable("id") long medicalStaffId) {
        return new ResponseEntity<MedicalStaff>(medicalStaffService.getMedicalStaffById(medicalStaffId), HttpStatus.OK);
    }

    //build update medicalStaff REST API
    @PutMapping("{id}")
    public ResponseEntity<MedicalStaff> updateMedicalStaff(@PathVariable("id") long medicalStaffID,
                                                 @RequestBody MedicalStaff medicalStaff) {
        return new ResponseEntity<MedicalStaff>(medicalStaffService.updateMedicalStaff(medicalStaff, medicalStaffID), HttpStatus.OK);
    }

    //build delete medicalStaff REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteMedicalStaff(@PathVariable("id") long id) {
        medicalStaffService.deleteMedicalStaff(id);
        return new ResponseEntity<String>("MedicalStaff deleted successfully!.", HttpStatus.OK);
    }
}
