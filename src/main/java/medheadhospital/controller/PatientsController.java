package medheadhospital.controller;

import medheadhospital.model.Patients;
import medheadhospital.service.PatientsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/patients")
public class PatientsController {

    private PatientsService patientsService;

    public PatientsController(PatientsService patientsService) {
        super();
        this.patientsService = patientsService;
    }

    //build create Patients REST API
    @PostMapping
    public ResponseEntity<Patients> savePatients(@RequestBody Patients Patients){
        return new ResponseEntity<Patients>(patientsService.savePatients(Patients), HttpStatus.CREATED);
    }

    //build gey all Patients REST API
    @GetMapping
    public List<Patients> getAllPatientss() {
        return patientsService.getAllPatients();
    }

    //build get by ID Patients REST API
    @GetMapping("{id}")
    public ResponseEntity<Patients> getPatientssById(@PathVariable("id") long PatientsId) {
        return new ResponseEntity<Patients>(patientsService.getPatientsById(PatientsId), HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public Patients getPatientByName(@PathVariable String name){
        return patientsService.findPatientByName(name);
    }
    //build update Patients REST API
    @PutMapping("{id}")
    public ResponseEntity<Patients> updatePatients(@PathVariable("id") long PatientsID,
                                                  @RequestBody Patients Patients) {
        return new ResponseEntity<Patients>(patientsService.updatePatients(Patients, PatientsID), HttpStatus.OK);
    }

    //build delete Patients REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deletePatients(@PathVariable("id") long id) {
        patientsService.deletePatients(id);
        return new ResponseEntity<String>("Patients deleted successfully!.", HttpStatus.OK);
    }
}
