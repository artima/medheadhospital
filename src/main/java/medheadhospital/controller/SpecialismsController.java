package medheadhospital.controller;

import medheadhospital.model.Specialisms;
import medheadhospital.service.SpecialismsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("/api/specialisms")
public class SpecialismsController {

    @Autowired
    private SpecialismsService specialismsService;

    public SpecialismsController(SpecialismsService specialismsService) {
        super();
        this.specialismsService = specialismsService;
    }

    //build create Specialisms REST API
    @PostMapping
    public ResponseEntity<Specialisms> saveSpecialisms(@RequestBody Specialisms specialisms){
        return new ResponseEntity<Specialisms>(specialismsService.saveSpecialisms(specialisms), HttpStatus.CREATED);
    }

    //build gey all Specialisms REST API
    @GetMapping
    public List<Specialisms> getAllSpecialisms() {
        return specialismsService.getAllSpecialisms();
    }

    //build get by ID Specialisms REST API
    @GetMapping("{id}")
    public ResponseEntity<Specialisms> getSpecialismssById(@PathVariable("id") long specialismsId) {
        return new ResponseEntity<Specialisms>(specialismsService.getSpecialismsById(specialismsId), HttpStatus.OK);
    }

    //build update Specialisms REST API
    @PutMapping("{id}")
    public ResponseEntity<Specialisms> updateSpecialisms(@PathVariable("id") long specialismsID,
                                                 @RequestBody Specialisms specialisms) {
        return new ResponseEntity<Specialisms>(specialismsService.updateSpecialisms(specialisms, specialismsID), HttpStatus.OK);
    }

    //build delete Specialisms REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteSpecialisms(@PathVariable("id") long id) {
        specialismsService.deleteSpecialisms(id);
        return new ResponseEntity<String>("Specialisms deleted successfully!.", HttpStatus.OK);
    }
}
