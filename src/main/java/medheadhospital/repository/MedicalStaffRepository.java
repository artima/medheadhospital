package medheadhospital.repository;

import medheadhospital.model.MedicalStaff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface MedicalStaffRepository  extends JpaRepository<MedicalStaff, Long> {

    @Query("SELECT CASE WHEN COUNT(g) > 0 THEN TRUE ELSE FALSE END FROM MedicalStaff g WHERE g.specialtyGroup = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT g FROM MedicalStaff g WHERE g.specialtyGroup = ?1")
    Optional<MedicalStaff> findByName(String name);
}