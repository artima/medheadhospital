package medheadhospital.repository;

import medheadhospital.model.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Optional;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {

    @Query(value = "SELECT h FROM Hospital h WHERE h.hospitalName LIKE %?1%")
    Hospital findByName(String name);

    @Query(value = "SELECT h.hospitalName FROM Hospital h JOIN h.specialisms s WHERE s.specialismName LIKE %?1%")
    List<String> findBySpeciality(String speciality);

    @Query("SELECT CASE WHEN COUNT(h) > 0 THEN TRUE ELSE FALSE END FROM Hospital h WHERE h.hospitalName = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT h.hospitalName FROM Hospital h WHERE h.hospitalName LIKE ?1")
    Optional<Hospital> findByNewName(String name);
}

