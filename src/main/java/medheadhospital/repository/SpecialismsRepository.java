package medheadhospital.repository;

import medheadhospital.model.Specialisms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface SpecialismsRepository  extends JpaRepository<Specialisms, Long> {

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN TRUE ELSE FALSE END FROM Specialisms s WHERE s.specialismName = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT s FROM Specialisms s WHERE s.specialismName = ?1")
    Optional<Specialisms> findByName(String name);
}
