package medheadhospital.repository;

import medheadhospital.model.Patients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface PatientsRepository extends JpaRepository<Patients, Long> {

    @Query(value = "SELECT p FROM Patients p WHERE p.lastName LIKE %?1%")
    Patients findByName(String name);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN TRUE ELSE FALSE END FROM Patients p WHERE p.lastName = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT p.lastName FROM Patients p WHERE p.lastName LIKE ?1")
    Optional<Patients> findByNewName(String name);
}
