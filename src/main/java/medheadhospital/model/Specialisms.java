package medheadhospital.model;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Data
@Entity
@Table(name = "specialisms")
public class Specialisms {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "specialism_name", nullable = false)
    private String specialismName;

    @ManyToOne
    @JoinColumn(name = "medical_staff")
    private MedicalStaff medicalStaff;
}
