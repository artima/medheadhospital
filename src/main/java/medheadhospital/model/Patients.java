package medheadhospital.model;

import lombok.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Data
@Entity
@Table(name = "patients")
public class Patients {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
     @Column(name = "first_name", nullable = false)
     private String firstName;
     @Column(name = "last_name")
     private String lastName;
     @Column(name = "email")
     private String email;
     @Column(name = "latitude")
     private Double latitude;
     @Column(name = "longitude")
     private Double longitude;

     @OneToMany
     @JoinColumn(name = "ps_id")
     private List<Specialisms> specialisms = new ArrayList<>();
}



