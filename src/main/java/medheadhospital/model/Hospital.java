package medheadhospital.model;

import lombok.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Data
@Entity
@Table(name = "hospital")
public class Hospital {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "hospital_name")
    private String hospitalName;
    @Column(name = "number_beds", nullable = false)
    private Integer numberBeds;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "hs_rid", referencedColumnName = "id")
    private List<Specialisms> specialisms = new ArrayList<>();
}
